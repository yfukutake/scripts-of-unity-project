﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;

public class PulseController : MonoBehaviour,ISaveable
{
    
    [SerializeField] Sliders speedOfPulsePerFlame;
    [SerializeField] TextMeshPro driverDebug;
    [SerializeField] Material SpatialMeshForMusic;
    public Material PurpleMusic
    {
        get { return SpatialMeshForMusic; }
        set { PurpleMusic = value; }
    }

    [SerializeField] GameObject obstaclesOfMusic;


    [SerializeField] Sliders pulseIntensity; 
    [SerializeField] Sliders pulsePeriod; 
    [SerializeField] TextMeshPro pulseRadialDebug;

    int pulseID;
    float _velocityOfPulsePerFlame; // saveable
    [Range(0,1)]float pulseOfPulseDriver = 0f;
    Coroutine drivePulse;

    float[] restore; // for restore On Enable


    void Awake() 
    {
        pulseID = Shader.PropertyToID("_Pulse_");
        autoID = Shader.PropertyToID("_Auto_Pulse_");
        intensityID = Shader.PropertyToID("_Intensity_");
        periodID = Shader.PropertyToID("_Period_");
    }

    
    public void PianoTriggeredPulse(float velocityPerPress) 
    {
        if (drivePulse != null) { return; }

        drivePulse = StartCoroutine(PartialPulseVelocity(velocityPerPress));

    }
 
    IEnumerator PartialPulseVelocity(float velocity) 
    {
        float targetNum = Mathf.Clamp01(pulseOfPulseDriver + velocity);

        if (velocity >= 0)  
        {
            while (pulseOfPulseDriver <= targetNum)
            {
                pulseOfPulseDriver += _velocityOfPulsePerFlame;
                PurpleMusic.SetFloat(pulseID, pulseOfPulseDriver);
                yield return null;
            }

            if (pulseOfPulseDriver >= 1)
            {
                pulseOfPulseDriver = 0;
            }
        } 
        else if (velocity < 0) 
        {
            while (pulseOfPulseDriver >= targetNum) 
            {
                pulseOfPulseDriver -= _velocityOfPulsePerFlame;
                PurpleMusic.SetFloat(pulseID, pulseOfPulseDriver);
                yield return null;
            }
            if (Mathf.Approximately(targetNum,0f))
            {
                pulseOfPulseDriver = 1;
            }
        }

        
        drivePulse = null;
    }
    
    public void OnSliderUpdatePulseDriver(SliderEventData eventData) 
    {
        PurpleMusic.SetFloat(pulseID, eventData.NewValue);
        driverDebug.text = eventData.NewValue.ToString();
    }
    public void OnSliderUpdatePulseSpeed(SliderEventData eventData) 
    {
        speedOfPulsePerFlame.SetOffset(eventData);
        if (!speedOfPulsePerFlame.isSelectedValue) { return; }
        _velocityOfPulsePerFlame = speedOfPulsePerFlame.CalculateSliderValueWithRange(eventData);
        speedOfPulsePerFlame.DisplayValues(eventData,driverDebug);
    }
    public void EnableMusicObstacles()
    {
        bool isActive = obstaclesOfMusic.activeInHierarchy;
        if (isActive)
        {
            obstaclesOfMusic.SetActive(false);
        }
        else if (!isActive)
        {
            obstaclesOfMusic.SetActive(true);
        }
    }

    int autoID;
    private void AutoPulseEnable(Material material, GameObject autoSlider)
    {
        float auto = material.GetFloat(autoID);

        if (Mathf.Approximately(auto, 1f))
        {
            material.SetFloat(autoID, 0f);
            if (autoSlider != null)
            {
                autoSlider.SetActive(false);
            }
            return;
        }
        else if (Mathf.Approximately(auto, 0f))
        {
            material.SetFloat(autoID, 1f);
            if (autoSlider != null)
            {
                autoSlider.SetActive(true);
            }
            return;
        }
    }
    [SerializeField] GameObject autoSpeedSlider;
    public void EnableAutoPulseForMusic()
    {
        AutoPulseEnable(PurpleMusic, autoSpeedSlider);
    }
    public void EnableAutoPulseGameBlackMat() 
    {
        AutoPulseEnable(GameBlackMat, null);
    }
    public void EnableAutoPulseGameTransparent() 
    {
        AutoPulseEnable(GameFlyPurpleMat, null);
    }


    [SerializeField] Material spatialGameMat;
    public Material GameBlackMat
    {
        get { return spatialGameMat; }
        set { GameBlackMat = value; }
    }
    [SerializeField] Material transparentGameMat;
    public Material GameFlyPurpleMat 
    {
        get { return transparentGameMat; }
        set { GameFlyPurpleMat = value; }
    }

    int intensityID;
    int periodID;
    float _musicMatIntensity, _musicMatPeriod; // for saveable
    public void UpdateIntensityOfPulseMusic(SliderEventData eventData)
    {
        UpdateFloatOfPulseMatProperty(eventData, pulseIntensity, PurpleMusic, intensityID, pulseRadialDebug);
        _musicMatIntensity = pulseIntensity.CalculateSliderValueWithRange(eventData);
    }
    public void UpdatePeriodOfPulseMusic(SliderEventData eventData)
    {
        UpdateFloatOfPulseMatProperty(eventData, pulsePeriod, PurpleMusic, periodID, pulseRadialDebug);
        _musicMatPeriod = pulsePeriod.CalculateSliderValueWithRange(eventData);
    }

    public bool isBlack; // changed by radial script
    float _gameBlackIntensity, _gameTransIntensity, _gameBlackPeriod, _gameTransPeriod; // for saveable
    public void UpdateIntensityOfGameBlack(SliderEventData eventData) 
    {
        if (!isBlack) return;
        UpdateFloatOfPulseMatProperty(eventData, pulseIntensity, GameBlackMat, intensityID, pulseRadialDebug);
        _gameBlackIntensity = pulseIntensity.CalculateSliderValueWithRange(eventData);
    }
    public void UpdateIntensityOfGameTransparent(SliderEventData eventData) 
    {
        if (isBlack) return;
        UpdateFloatOfPulseMatProperty(eventData, pulseIntensity, GameFlyPurpleMat, intensityID, pulseRadialDebug);
        _gameTransIntensity = pulseIntensity.CalculateSliderValueWithRange(eventData);
    }
    public void UpdatePeriodOfGameMat(SliderEventData eventData) 
    {
        if (!isBlack) return;
        UpdateFloatOfPulseMatProperty(eventData, pulsePeriod, GameBlackMat, periodID, pulseRadialDebug);
        _gameBlackPeriod = pulsePeriod.CalculateSliderValueWithRange(eventData);
    }
    public void UpdatePeriodOfGameTransparent(SliderEventData eventData) 
    {
        if (isBlack) return;
        UpdateFloatOfPulseMatProperty(eventData, pulsePeriod, GameFlyPurpleMat, periodID, pulseRadialDebug);
        _gameTransPeriod = pulsePeriod.CalculateSliderValueWithRange(eventData);
    }


    private void UpdateFloatOfPulseMatProperty(SliderEventData eventData, Sliders sliders, Material targetMat, int targetProperty, TextMeshPro debug) 
    {
        sliders.SetOffset(eventData);
        if (!sliders.isSelectedValue) { return; }
        float updatedNum = sliders.CalculateSliderValueWithRange(eventData);

        if (targetMat.HasProperty(targetProperty))
        {
            targetMat.SetFloat(targetProperty, updatedNum);
        }
        sliders.DisplayValues(eventData, debug);
        
    }

    float[] CreateIndex() 
    {
        float[] matProperties = new float[7];


        if (PurpleMusic != null)
        {
            matProperties[0] = _velocityOfPulsePerFlame;
            matProperties[1] = _musicMatIntensity;
            matProperties[2] = _musicMatPeriod;
        }
        else
        {
            matProperties[3] = _gameBlackIntensity;
            matProperties[4] = _gameBlackPeriod;
            matProperties[5] = _gameTransIntensity;
            matProperties[6] = _gameTransPeriod;

        }

        return matProperties;
    }

    public void InitializeOnStart()
    {
        if (PurpleMusic != null)
        {
            pulseOfPulseDriver = 0;
        }
    }

    public object CaptureState()
    {
        return CreateIndex();
    }

    public void RestoreState(object state)
    {
        if (state == null) { return; }

        restore = state as float[]; // for OnEnbale Restore

        if (restore == null) 
        {
            Debug.LogError("Pulse Dictionary is null, somethin wrong");
            return;
        }
        if (PurpleMusic != null)
        {
            _velocityOfPulsePerFlame = restore[0];
            PurpleMusic.SetFloat( intensityID, restore[1]);
            PurpleMusic.SetFloat( periodID, restore[2]); //
        }
        else 
        {
            GameBlackMat.SetFloat(intensityID, restore[3]);
            GameBlackMat.SetFloat(periodID, restore[4]);
            GameFlyPurpleMat.SetFloat(intensityID, restore[5]); //
            GameFlyPurpleMat.SetFloat(periodID, restore[6]); //
        }
        
    }
    public void RestoreOnEnable() 
    {
        if (restore == null) return;

        if (PurpleMusic != null)
        { 
            PurpleMusic.SetFloat(periodID, restore[2]);    
            Debug.Log("Music Auto is Restored on Enable!");
            
        }
        else 
        {
            GameFlyPurpleMat.SetFloat(intensityID, restore[5]);    
            GameFlyPurpleMat.SetFloat(periodID, restore[6]);   
        }
    }
}
