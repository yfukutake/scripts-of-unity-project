﻿using Microsoft.MixedReality.Toolkit.UI;
using TMPro;
using UnityEngine;

[CreateAssetMenu(fileName = "Slider", menuName = "sliders/Make New Slider", order = 0)]
public class Sliders : ScriptableObject 
{
    public bool isSelectedValue;
    [SerializeField] ValueSet valueSet;

    private float offset;
    private bool onStart = true;

    [System.Serializable]
    public class ValueSet 
    {
        public string Name;
        public float initializedValue;
        public float multiplier;
        public float SliderValueAsStartingPoint = 0f;
        public float minimumNumber;
        public float maxNumber;
    }
    public float CalculateSliderValue(SliderEventData eventData) 
    {
        return (eventData.NewValue - valueSet.SliderValueAsStartingPoint) * offset * valueSet.multiplier;   
    }
    public float CalculateSliderValueWithRange(SliderEventData eventData) 
    {
        return Mathf.Clamp((eventData.NewValue - valueSet.SliderValueAsStartingPoint) * offset * valueSet.multiplier,valueSet.minimumNumber,valueSet.maxNumber);
    }
    public void SetOffset(SliderEventData eventData) 
    {
        if (onStart)
        {
            offset = valueSet.initializedValue / eventData.NewValue;
            onStart = false;
        }
    }
    public void SetOffsetWithPreparedData(float preparedOffset) 
    {
        if (onStart) 
        {
            offset = preparedOffset;
            onStart = false;
        }
    }
    public void DisplayValues(SliderEventData eventData, TextMeshPro textMeshPro) 
    {
        textMeshPro.text = string.Format("{0}:{1}", valueSet.Name, CalculateSliderValue(eventData));
    }
    public void DebugDisplay(SliderEventData eventData, TextMeshPro textMeshPro)
    {
        textMeshPro.text = string.Format("{0}:{1} data:{2} offset:{3}", valueSet.Name, CalculateSliderValue(eventData), eventData.NewValue, offset);
    }

}