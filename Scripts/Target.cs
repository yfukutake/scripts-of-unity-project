﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    ParticleSystem showerFireworks;
    AudioSource audioSource;
    SkyboxManager sky;
    void Start()
    {
        showerFireworks = GetComponentInChildren<ParticleSystem>();
        showerFireworks.Stop();
        audioSource = GetComponentInChildren<AudioSource>();
        sky = GameObject.Find("Managers").GetComponentInChildren<SkyboxManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        SpawnedObject spawnedObject = other.GetComponent<SpawnedObject>();
        if (spawnedObject == null) { return; }

        StartCoroutine(OnTriggerBehavior(spawnedObject));
        //updating UI or just flashing UI?
    }
    IEnumerator OnTriggerBehavior(SpawnedObject sp)
    {

        yield return sp.TriggerExplosion();

        audioSource.gameObject.transform.position = new Vector3(0,1f,0.5f);
        audioSource.Play();

        sky.SwapSubSky();

        showerFireworks.Play();
        yield return new WaitForSeconds(audioSource.clip.length);
        showerFireworks.Stop();

    }
}
