﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxManager : MonoBehaviour, ISaveable
{

    [SerializeField] Material[] skys;
    [SerializeField] Material[] subSkys;
    [SerializeField] Renderer insideSky, outsideSky;
    [SerializeField] Animator clippingAnim;
    AudioSource animSound;
    Coroutine swapMaterial;
    bool isSubSky;
    int index;
    int subIndex;

    public void SwapSky()
    {
        if (swapMaterial != null) { return; }

        index++;
        if (index >= skys.Length) { index = 0; }
        swapMaterial = StartCoroutine(LerpMaterials(index, skys));

        isSubSky = false;
    }
    public void SwapSubSky()
    {
        if (swapMaterial != null) { return; }

        subIndex++;
        if (subIndex >= subSkys.Length) { subIndex = 0; }
        swapMaterial = StartCoroutine(LerpMaterials(subIndex, subSkys));

        isSubSky = true;
    }

    IEnumerator LerpMaterials(int _index, Material[] sky)
    {
        outsideSky.material = sky[_index];
        int lerpBoolParam = Animator.StringToHash("LerpSky"); 
        clippingAnim.SetBool(lerpBoolParam, true);
        animSound.Play();
        yield return null; 

        Debug.Log(clippingAnim.GetCurrentAnimatorStateInfo(0).fullPathHash);


        while (true)
        {
            float isDoneAnim = clippingAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            Debug.Log(isDoneAnim);
            yield return null;

            if (isDoneAnim >= 1f) 
            {
                Debug.Log("break the while loop");
                break;
            }
        }

        insideSky.material = sky[_index];
        animSound.Stop();
        clippingAnim.SetBool(lerpBoolParam, false);

        Debug.Log("animation bool is" + clippingAnim.GetBool(lerpBoolParam));

        swapMaterial = null;

    }

    public void InitializeOnStart()
    {
        index = 0;
        subIndex = 0;
        isSubSky = false;
        swapMaterial = null;
        animSound = GetComponentInChildren<AudioSource>();
    }

    public object CaptureState()
    {
        int[] skyStatus = new int[3];

        if (!isSubSky)
        {
            skyStatus[0] = 0;
        }
        else
        {
            skyStatus[0] = 1;
        }
        skyStatus[1] = index;
        skyStatus[2] = subIndex;
 
        return skyStatus;
    }

    public void RestoreState(object state)
    {
        if (state == null) return;

        int[] restore = (int[])state;

        if (restore[0] == 0)
        {
            insideSky.material = skys[restore[1]];
        }
        else if (restore[0] == 1) 
        {
            insideSky.material = subSkys[restore[2]];
            isSubSky = true;
        }

        index = restore[1];
        subIndex = restore[2];
    }
}
