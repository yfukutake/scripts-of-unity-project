﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;


public class SliderValueManager : MonoBehaviour
{
    
    [SerializeField] Sliders spawnablesScale;
    [SerializeField] Sliders spawnablesSpeed;

    [SerializeField] TextMeshPro debug;
    SpawnManager _spawnManager;



    void Start()
    {
        _spawnManager = FindObjectOfType<SpawnManager>();
 

    }

    
    public void UpdateScaleOfSpawnables(SliderEventData eventData) 
    {
        spawnablesScale.SetOffset(eventData);
        if (!spawnablesScale.isSelectedValue) { return; }
        _spawnManager.scale = spawnablesScale.CalculateSliderValue(eventData);
        spawnablesScale.DisplayValues(eventData,debug);

    }
    public void UpdateSpeedOfSpawnables(SliderEventData eventData) 
    {
        spawnablesSpeed.SetOffset(eventData);
        if (!spawnablesSpeed.isSelectedValue) { return; }
        _spawnManager.spawnSpeed = spawnablesSpeed.CalculateSliderValue(eventData);
        spawnablesSpeed.DisplayValues(eventData,debug);
    }


}
