﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreOnEnable : MonoBehaviour
{

    PulseController pulseController;
    IEnumerator Start()
    {
        
        pulseController = GameObject.FindWithTag("Pulse").GetComponent<PulseController>();
        Debug.Log("Right Before RestoreOnEnable before yield return null");
        yield return null;
        
        pulseController.RestoreOnEnable();

    }
 

}
