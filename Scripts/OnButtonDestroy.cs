﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnButtonDestroy : MonoBehaviour
{
    SpawnedObject[] spawnedObjects;
    public void OnPushedButtonDestroy()
    {
        spawnedObjects = GetComponentsInChildren<SpawnedObject>();
        foreach (SpawnedObject spawnedObject in spawnedObjects)
        {
            spawnedObject.OnPressedDestroy();
        }
    }
}
